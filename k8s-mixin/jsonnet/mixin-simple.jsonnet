local utils = import "../vendor/kubernetes-mixin/lib/utils.libsonnet"; 
local conf = (import "../vendor/kubernetes-mixin/mixin.libsonnet") 
+ 
{ 
  _config+:: { 
    kubeStateMetricsSelector: 'job="kube-state-metrics"', 
    cadvisorSelector: 'job="kubernetes-cadvisor"', 
    nodeExporterSelector: 'job="kubernetes-node-exporter"', 
    kubeletSelector: 'job="kubernetes-kubelet"', 
    grafanaK8s+:: { 
      dashboardNamePrefix: 'Mixin / ', 
      dashboardTags: ['kubernetes', 'infrastucture'], 
    }, 
    prometheusAlerts+:: { 
        groups: ['kubernetes-resources'], 
    } 
  }, 
} 
+ { 
prometheusAlerts+:: { 
    /*  From Alerts select only the 'kubernetes-system': 
        https://github.com/kubernetes-monitoring/kubernetes-mixin/blob/master/alerts/system_alerts.libsonnet#L9 
    */ 
    groups: 
        std.filter( 
            function(alertGroup) 
              alertGroup.name == 'kubernetes-system' 
            , super.groups 
        ) 
    } 
} 
+ { 
prometheusRules+::  { 
    /*  From Rules select only the 'k8s.rules': 
        https://github.com/kubernetes-monitoring/kubernetes-mixin/blob/master/rules/apps.libsonnet#L10 
    */ 
    groups: 
        std.filter( 
            function(alertGroup) 
              alertGroup.name == 'k8s.rules' 
            , super.groups 
        ) 
    } 
} 
+ { 
    grafanaDashboards+:: {} 
} 
; 
{ ['prometheus-alerts']: conf.prometheusAlerts } 
+ 
{ ['prometheus-rules']: conf.prometheusRules } 
+ 
{ ['grafana-dashboard']: conf.grafanaDashboards['kubelet.json'], }
