module "acs_1004_dev_gitlab" {
    label = "acs_1004_dev_gitlab"
    source = "./module/dockerhost"
    type = "g6-dedicated-8"
    authorized_keys = var.authorized_keys
    token = var.token
}

module "acs_1004_dev_k8s" {
    label = "acs_1004_dev_k8s"
    source = "./module/dockerhost"
    type = "g6-dedicated-50"
    authorized_keys = var.authorized_keys
    token = var.token
}

module "acs_1004_dev_fw" {
    firewall_label = "acs_1004_dev_fw"
    source = "./module/firewall"
    tags = [ "1000"]
    linodes = [module.acs_1004_dev_gitlab.linode_instance_id,module.acs_1004_dev_k8s.linode_instance_id]
}

module "acs_lke_test" {
    source = "./module/k8s"
    tags = [ "dev"]
    label = "devcluster"
    token = var.token
    pools = [
    {
        type = "g6-standard-1"
        max = 1
    }
    ]

}