terraform {
    required_providers {
      linode = {
          source = "linode/linode"
          version = "1.25.1"
      }
    }

    backend "remote" {
        organization = "alligator-company"

        workspaces {
            name = "acs-dmstack"
        }
    }

}

provider "linode" {
    token = var.token
}
