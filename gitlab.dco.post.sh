#!/bin/bash

[[ -f ~/.env ]] && . ~/.env
SSHLOGIN="$DOCKERUSER@$DOCKERHOST"

register_token=$(docker exec -ti gitlab gitlab-rails runner -e production "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token")

kubectl --namespace argo-events delete secret gitlab-cert
kubectl --namespace argo-events delete secret gitlab-runner-gitlab-runner
kubectl --namespace argo-events create secret generic gitlab-cert --from-file $GITLABDOMAIN.crt=/tmp/cert.crt
kubectl --namespace argo-events create secret generic gitlab-runner-gitlab-runner \
    --from-literal runner-registration-token=$register_token \
    --from-literal runner-token="" \
    --from-literal runner-registration-url="https://$GITLABDOMAIN"
kubectl --namespace argo-events rollout restart deployments/gitlab-runner-gitlab-runner

rootpwd=$(ssh "$SSHLOGIN" "cat ~/gitlab/data/config/initial_root_password |grep 'Password:' |awk -F: '{print $2}' | sed 's/ //g'")
echo $rootpwd
