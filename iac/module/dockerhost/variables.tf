variable "image" {
  description = "Image to use for Linode instance"
  default = "linode/debian10"
}

variable "label" {
  description = "The Linode's label is for display purposes only."
  default = "default-linode"
}

variable "region" {
  description = "The region where your Linode will be located."
  default = "eu-central"
}

variable "type" {
  description = "Your Linode's plan type."
  default = "g6-standard-1"
}

variable "token" {}
variable "authorized_keys" {}
