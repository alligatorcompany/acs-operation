#!/bin/bash
set -euo pipefail

SCRIPTDIR=$(dirname "$0")

if kubectl get ns $1 > /dev/null 2>&1; then
    echo "Please provide non existent namespace"
    kubectl get ns
    exit -2
fi
export ENVNAME=$1
if ! kubectl create namespace $ENVNAME; then
    echo "Could not create namespace $ENVNAME"
    exit -1
fi

envsubst < $SCRIPTDIR/yml/rbac.admin.yaml | kubectl apply -f -
$SCRIPTDIR/secrets/create_from_env.sh $ENVNAME 
echo "Secrets created."

kubectl patch --namespace $ENVNAME serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}'

kustomize build $SCRIPTDIR/yml/mssql/env/dev | ENVNAME=$ENVNAME DOMAINNAME=$DOMAINNAME envsubst | kubectl -n $ENVNAME apply -f -
echo "Waiting for mssql to startup..."
kubectl -n $ENVNAME wait -l statefulset.kubernetes.io/pod-name=mssql-0 pod --for condition=ready --timeout=1800s

echo "Setup mssql for dvb"
argo -n $ENVNAME submit $SCRIPTDIR/yml/install.ms.yaml --log

echo "Waiting for dvb to startup..."
kustomize build $SCRIPTDIR/yml/dvb/env/dev | envsubst | kubectl -n $ENVNAME apply -f -
kubectl -n $ENVNAME rollout status deployment dvb
ENVNAME=$ENVNAME envsubst < $SCRIPTDIR/yml/ingress.tmpl.yaml | kubectl apply -n $ENVNAME -f -

echo "$ENVNAME created."
