#!/usr/bin/env sh
NS=default
if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dataloader-credentials')" ]; then
    kubectl delete secret dataloader-credentials --namespace=$NS
fi
kubectl create secret generic dataloader-credentials \
    --namespace=$NS \
    --from-literal=encpass=$(cat dvbpass | base64) \
    --from-file=dbuser=dbuser \
    --from-file=dbpass=dbpass \
    --from-file=dvbuser=dvbuser \
    --from-file=dvbpass=dvbpass \
    --from-file=exasol-url=exasol_url \
    --from-file=dvb-url=dvb_url \
    --from-file=exasol-host=exasol_host \
    --from-file=exasol-port=exasol_port

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'gitlab-deploykey')" ]; then
    kubectl delete secret gitlab-deploykey --namespace=$NS
fi
kubectl create secret generic gitlab-deploykey \
    --namespace=$NS \
    --from-file=id_rsa=id_rsa \
    --from-file=id_rsa.pub=id_rsa.pub \
    --from-file=known_hosts=known_hosts

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dvb-secrets')" ]; then
    kubectl delete secret dvb-secrets --namespace=$NS
fi
kubectl create secret generic dvb-secrets \
    --namespace=$NS \
    --from-file=systems_password_private_key_password=systems_password_private_key_password \
    --from-file=systems_password_public_key=systems_password_public_key \
    --from-file=authenticator_password=authenticator_password \
    --from-file=core_dbadmin_password=core_dbadmin_password \
    --from-file=datavault_builder_license=datavault_builder_license \
    --from-file=scheduler_password=scheduler_password \
    --from-file=systems_password_private_key=systems_password_private_key

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'db-secrets')" ]; then
    kubectl delete secret db-secrets --namespace=$NS
fi
kubectl create secret generic db-secrets \
	--namespace=$NS \
    --from-file=db_sysuser=dbuser \
	--from-file=db_syspass=dbpass

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'regcred')" ]; then
    kubectl delete secret regcred --namespace=$NS
fi
kubectl create secret docker-registry regcred \
    --namespace=$NS \
    --docker-server=docker.datavault-builder.com \
    --docker-username=$(cat dvb_pull_user) \
    --docker-password=$(cat dvb_pull_pass)

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'gitlab-pull')" ]; then
    kubectl delete secret gitlab-pull --namespace=$NS
fi
kubectl create secret docker-registry gitlab-pull \
    --namespace=$NS \
    --docker-server=registry.gitlab.com  \
    --docker-username=$(cat gitlab_pull_user) \
    --docker-password=$(cat gitlab_pull_pass)
