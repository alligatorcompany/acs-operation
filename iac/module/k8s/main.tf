terraform {
    required_providers {
      linode = {
          source = "linode/linode"
          version = "1.25.1"
      }
    }

}
resource "linode_lke_cluster" "lkeplane" {
    k8s_version = var.k8s_version
    label = var.label
    region = var.region
    tags = var.tags

    dynamic "pool" {
        for_each = var.pools
        content {
            type  = pool.value["type"]
            count = 1
            autoscaler {
               min = 1
               max = pool.value["max"]
            }
        }
    }
}
