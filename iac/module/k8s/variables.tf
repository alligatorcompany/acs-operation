variable "k8s_version" {
    description = "The Kubernetes version to use for this cluster. (required)"
    default = "1.22"
}

variable "region" {
    description = "The region where your cluster will be located. (required)"
    default = "eu-central"
}

variable "pools" {
    description = "The Node Pool specifications for the Kubernetes cluster. (required)"
    type = list(object({
    type = string
    max = number
    }))
}
variable "tags" {
  description = "List of tags to apply to the cluster"
  type        = list(string)
  default     = []
}

variable "label" {}

variable "token" {}