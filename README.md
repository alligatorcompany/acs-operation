# Preparation

- kubectl installation
- tobs cli installation https://github.com/timescale/tobs/releases/tag/0.6.1
- k3d
- kustomize
- docker

k3d 4.4.8 benötigt kein vorangestelltes DOCKER_HOST mehr?
tobs 0.6.1 timescaledb failed to start permission denied

# Secrets & Credentials

- db-secrets
Exasol User
Exasol Password
Exasol URL

- dvb-secrets
DVB user
DVB password
DVB url

- regcred
dockerjson pull secrets

- deploy-key
id_rsa
id_rsa.pub
known_hosts

# Post
gitlab runner configuration

workflow cron einrichten

workflwo daily ggf. anpassen
