#!/bin/bash

[[ -f ~/.env ]] && . ~/.env
SSHLOGIN="$DOCKERUSER@$DOCKERHOST"

cat > /tmp/env <<EOF
DOCKERUSER=$DOCKERUSER
DOCKERHOST=$DOCKERHOST
DOCKERIP=$DOCKERIP
DOCKERURL=$DOCKERURL
APIPORT=$APIPORT
HTTPSPORT=$HTTPSPORT
SSHPORT=$SSHPORT
ADDDNS=$ADDDNS
STAGE=$STAGE
DOMAINNAME=$DOMAINNAME
GITLABDOMAIN=$GITLABDOMAIN
REGISTRYDOMAIN=$REGISTRYDOMAIN
MINIODOMAIN=$MINIODOMAIN
TRAEFIKDOMAIN=$TRAEFIKDOMAIN
TRAEFIKAUTH=$TRAEFIKAUTH
EOF

./keycreate

scp -r gitlab "$SSHLOGIN":~/
scp -r /tmp/env "$SSHLOGIN":~/gitlab/.env
ssh "$SSHLOGIN" "mkdir -p ~/gitlab/certs"
scp /tmp/cert.crt "$SSHLOGIN":~/gitlab/certs/cert.crt
scp /tmp/cert.key "$SSHLOGIN":~/gitlab/certs/cert.key

ssh "$SSHLOGIN" "cd ~/gitlab/ && docker-compose up -d"
while :
do
    healthy=$(DOCKER_HOST="$DOCKERURL" docker container inspect gitlab |grep -i healthy|wc -l)
    [[ ${healthy} -gt 0 ]] && { break ; }
    echo "Starting up... this may take a few minutes ..."
    sleep 20
done

