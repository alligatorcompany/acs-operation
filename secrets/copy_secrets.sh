#!/usr/bin/env sh

ENVNAME=$1
kubectl get secret db-secrets --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret gitlab-deploykey --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret dvb-secrets --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret exasol-license --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret regcred --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret dataloader-credentials --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
kubectl get secret gitlab-pull --namespace default -o yaml | sed "s/namespace: default/namespace: $ENVNAME/g" | kubectl apply -n $ENVNAME -f -
