terraform {
    required_providers {
      linode = {
          source = "linode/linode"
          version = "1.25.1"
      }
    }

}

resource "random_string" "root_password" {
    length  = 32
    special = true
}

resource "linode_instance" "dockerhost" {
    image = var.image
    label = var.label
    region = var.region
    type = var.type
    authorized_keys = var.authorized_keys
    root_pass = random_string.root_password.result
    stackscript_id = 607433
    backups_enabled = true
}