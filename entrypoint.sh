#!/usr/bin/zsh
set -a && export DOCKER_HOST=$DOCKERURL && set +a
if [[ $# -gt 0 ]]
then
    "$@"
else
    zsh
fi
