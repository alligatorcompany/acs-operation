//Export this cluster's attributes
output "kubeconfig" {
   value = linode_lke_cluster.lkeplane.kubeconfig
}

output "api_endpoints" {
   value = linode_lke_cluster.lkeplane.api_endpoints
}

output "status" {
   value = linode_lke_cluster.lkeplane.status
}

output "id" {
   value = linode_lke_cluster.lkeplane.id
}

output "pool" {
   value = linode_lke_cluster.lkeplane.pool
}