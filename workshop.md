# Workshop I - Entwicklungsumgebung und CI

- Vorstellung Entwicklungsumgebung
- Continuous Integration mit Gitlab
- Vorstellung der Modellimplementierung Kreditangebot
- Erweiterung der Implementierung um Userdaten aus Marketo und Salesforce

# Workshop II - Workflow und Microbatching

- Argo Workflow
- Deployment auf der Exasol SaaS
- Microbatching mit Kafka Messages

# Workshop III - Data Produkte erstellen und Data Lineage

- Erstellung von Datenprodukten als Basis für die BI Frontends
- Data LIneage als Basis für Impact und Herkunftsanalysen - inklusive Attributlevel
