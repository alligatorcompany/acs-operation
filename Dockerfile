FROM alligatorcompany/acs-deploy:latest as acs-deploy

FROM debian:bullseye-slim as acs-operation

RUN apt -qq update && apt -qq install build-essential libpq-dev libffi-dev zsh git curl \
    openssh-client less libperl5.32 perl-doc sqlite3 libpq5 postgresql-client  \
    ca-certificates fonts-powerline tmux asciidoctor unixodbc jq htop unzip \
    python3 python3-pip \
    -y --no-install-recommends > /dev/null

# Install EXAplus and the ODDB driver & config.
COPY --from=acs-deploy /opt/exaplus /opt/exaplus/
COPY --from=acs-deploy /opt/exasol/odbc/lib /opt/exasol/odbc/lib
COPY --from=acs-deploy /bin/snowsql /bin/snowsql

COPY --from=acs-deploy /usr/lib/snowflake/odbc/ /usr/lib/snowflake/odbc/
COPY --from=acs-deploy /etc/odbc* /etc/
COPY --from=acs-deploy /bin/sqitch /bin/sqitch
COPY --from=acs-deploy /lib/perl5 /lib/perl5
COPY --from=acs-deploy /etc/sqitch /etc/sqitch/

RUN curl -LO https://github.com/k3d-io/k3d/releases/download/v5.4.1/k3d-linux-amd64 && \
    mv k3d-linux-amd64 /bin/k3d && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.5/bin/linux/amd64/kubectl && mv kubectl /bin/  && \
    curl -LO https://github.com/peak/s5cmd/releases/download/v1.4.0/s5cmd_1.4.0_Linux-64bit.tar.gz && \
    tar xfz s5cmd*gz s5cmd && mv s5cmd /bin/ && rm *gz  && \
    curl -LO https://github.com/gohugoio/hugo/releases/download/v0.96.0/hugo_extended_0.96.0_Linux-64bit.tar.gz && \
    tar xfz hugo*gz hugo && mv hugo /bin/ && rm *gz  && \
    curl -LO https://github.com/argoproj/argo-workflows/releases/download/v3.3.1/argo-linux-amd64.gz  && \
    gunzip argo* && mv argo* /bin/argo  && \
    curl -LO https://download.docker.com/linux/static/stable/x86_64/docker-20.10.14.tgz && \
    tar xfz docker-*.tgz docker/docker && mv docker/docker /bin/ && rm *.tgz && rmdir docker && \
    curl -LO https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Linux_x86_64.tar.gz && \
    tar xfz k9s*.tar.gz k9s && mv k9s /bin/ && rm *.tar.gz  && \
    curl -LO https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/kubeseal-0.17.3-linux-amd64.tar.gz && \
    tar xfz kube*.tar.gz && mv kubeseal /bin/kubeseal && \
    curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash && \
    mv kustomize /bin/kustomize && \
    curl -LO https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && \
    unzip terraform*.zip && mv terraform /bin/terraform && rm terraform*zip && \
    curl -LO https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx_v0.9.4_linux_x86_64.tar.gz && \
    curl -LO https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz && \
    curl -LO https://github.com/kvaps/kubectl-build/raw/master/kubectl-build && \
    tar xfz kubens*tar.gz && tar xfz kubectx*tar.gz && rm *.tar.gz && chmod 0755 kube* && mv kube* /bin/ && \
    curl -LO https://github.com/okteto/okteto/releases/download/2.0.2/okteto-Linux-x86_64 && \
    chmod +x okteto-Linux-x86_64 && mv okteto-Linux-x86_64 /bin/okteto && \
    curl -LO https://github.com/timescale/tobs/releases/download/0.9.0/tobs_0.9.0_Linux_x86_64 && \
    chmod +x tobs_0.9.0_Linux_x86_64 && mv tobs_0.9.0_Linux_x86_64 /bin/tobs
COPY env* /bin/
COPY stage* /bin/
COPY build /bin/
RUN mkdir -p /bin/yml/dvb
RUN mkdir -p /bin/yml/exasol
COPY yml/exasol/ /bin/yml/exasol/
COPY yml/dvb/ /bin/yml/dvb/
COPY yml/stage/ /bin/yml/stage/
COPY yml/*.yml /bin/yml/
COPY yml/*.yaml /bin/yml/
COPY git* /bin/

RUN chmod 0755 /bin/*
RUN ln -s /opt/exaplus/exaplus /bin/exaplus

FROM debian:bullseye-slim

RUN apt -qq update && apt -qq install libpq-dev libffi-dev git curl zsh vim nano \
    openssh-client less libperl5.32 perl-doc sqlite3 libpq5 postgresql-client asciidoctor asciidoctor-doc  \
    git-core gnupg locales wget gettext parallel make \
    python3 python3-pip build-essential unixodbc-dev python3-dev \
    ca-certificates fonts-powerline tmux asciidoctor unixodbc jq htop unzip \
    -y --no-install-recommends > /dev/null && \
    pip install -q dbt==0.21.1 dbt-exasol==1.0.3 sqlfluff==0.8.2 markupsafe==2.0.1 dwhgen --no-cache-dir 

RUN mkdir -p /usr/share/man/man1/ && apt -qq install openjdk-11-jre-headless -y

# Install EXAplus and the ODDB driver & config.
COPY --from=acs-operation /opt/exaplus /opt/exaplus/
COPY --from=acs-operation /opt/exasol/odbc/lib /opt/exasol/odbc/lib
COPY --from=acs-operation /bin/snowsql /bin/snowsql

COPY --from=acs-operation /usr/lib/snowflake/odbc/ /usr/lib/snowflake/odbc/
COPY --from=acs-operation /etc/odbc* /etc/
COPY --from=acs-operation /bin/sqitch /bin/sqitch
COPY --from=acs-operation /lib/perl5 /lib/perl5
COPY --from=acs-operation /etc/sqitch /etc/sqitch/
COPY --from=acs-deploy /etc/odbcinst.ini /etc/odbcinst.ini
RUN locale-gen en_US.UTF-8

# run the installation script
RUN ZSH=/etc/oh-my-zsh sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)" | zsh | true
COPY zshrc /etc/zsh/zshrc
RUN curl -LO https://github.com/superbrothers/zsh-kubectl-prompt/archive/refs/tags/v1.4.0.tar.gz && \
    mkdir /etc/zsh-kubectl-prompt/ && \
    tar xvfz v1.4.0.tar.gz --strip 1 -C /etc/zsh-kubectl-prompt/ && \
    chmod 0755 /etc/zsh-kubectl-prompt/*

COPY --from=acs-operation /bin /bin
COPY entrypoint.sh /bin/entrypoint.sh
RUN chmod 0755 /bin/*

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN /bin/snowsql >/dev/null 2>&1
ENV PYTHONIOENCODING=utf-8
ENTRYPOINT ["/bin/entrypoint.sh"]
