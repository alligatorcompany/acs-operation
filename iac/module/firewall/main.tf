terraform {
    required_providers {
      linode = {
          source = "linode/linode"
          version = "1.25.1"
      }
    }

}

resource "linode_firewall" "acs_server" {
  label = var.firewall_label
  tags  = var.tags

  inbound {
    label = "https"
    action = "ACCEPT"
    protocol = "TCP"
    ports = "443"
    ipv4 = ["0.0.0.0/0"]
  }

  inbound {
    label = "k8s-api"
    action = "ACCEPT"
    protocol = "TCP"
    ports = "1666"
    ipv4 = ["0.0.0.0/0"]
  }

  inbound {
    label = "ssh"
    action = "ACCEPT"
    protocol = "TCP"
    ports = "22"
    ipv4 = ["0.0.0.0/0"]
  }

  inbound {
    label = "gitshell"
    action = "ACCEPT"
    protocol = "TCP"
    ports = "2222"
    ipv4 = ["0.0.0.0/0"]
  }

  inbound_policy = "DROP"
  outbound_policy = "ACCEPT"

  linodes = var.linodes
}