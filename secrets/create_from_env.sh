#!/usr/bin/env sh
set -euo pipefail
NS=$1
if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dataloader-credentials')" ]; then
    kubectl delete secret dataloader-credentials --namespace=$NS
fi
kubectl create secret generic dataloader-credentials \
    --namespace=$NS \
    --from-literal=encpass="$(echo $DVB_PASS | base64)" \
    --from-literal=dbuser="$(echo $DB_USER)" \
    --from-literal=dbpass="$(echo $DB_PASS)" \
    --from-literal=dvbuser="$(echo $DVB_USER)" \
    --from-literal=dvbpass="$(echo $DVB_PASS)" \
    --from-literal=db-url="$(echo $DB_URL)" \
    --from-literal=dvb-url="$(echo $DVB_URL)" \
    --from-literal=db-host="$(echo $DB_HOST)" \
    --from-literal=db-port="$(echo $DB_PORT)"

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'gitlab-deploykey')" ]; then
    kubectl delete secret gitlab-deploykey --namespace=$NS
fi
kubectl create secret generic gitlab-deploykey \
    --namespace=$NS \
    --from-literal=id_rsa="$(echo $ID_RSA)" \
    --from-literal=id_rsa.pub="$(echo $ID_RSA_PUB)" \
    --from-literal=known_hosts="$(echo $KNOWN_HOSTS)"

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'dvb-secrets')" ]; then
    kubectl delete secret dvb-secrets --namespace=$NS
fi
kubectl create secret generic dvb-secrets \
    --namespace=$NS \
    --from-literal=systems_password_private_key_password="$(echo $DVB_PRIVATEKEY_PASS)" \
    --from-literal=systems_password_public_key="$(echo $DVB_PUBLICKEY)" \
    --from-literal=authenticator_password="$(echo $DVB_AUTHENTICATOR_PASS)" \
    --from-literal=core_dbadmin_password="$(echo $DVB_DBADMIN_PASS)" \
    --from-literal=datavault_builder_license="$(echo $DVB_LICENSE)" \
    --from-literal=scheduler_password="$(echo $DVB_SCHEDULER_PASS)" \
    --from-literal=systems_password_private_key="$(echo $DVB_PRIVATEKEY)"

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'db-secrets')" ]; then
    kubectl delete secret db-secrets --namespace=$NS
fi
kubectl create secret generic db-secrets \
	--namespace=$NS \
  --from-literal=db_sysuser="$(echo $DB_SYSUSER)" \
  --from-literal=db_syspass="$(echo $DB_SYSPASS)"

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'regcred')" ]; then
    kubectl delete secret regcred --namespace=$NS
fi
kubectl create secret docker-registry regcred \
    --namespace=$NS \
    --docker-server="$(echo $DVB_PULL_URL)" \
    --docker-username="$(echo $DVB_PULL_USER)" \
    --docker-password="$(echo $DVB_PULL_PASS)"

if [ ! -z "$(kubectl get secret --namespace=$NS | grep 'gitlab-pull')" ]; then
    kubectl delete secret gitlab-pull --namespace=$NS
fi
kubectl create secret docker-registry gitlab-pull \
    --namespace=$NS \
    --docker-server="$(echo $REGISTRY_URL)"  \
    --docker-username="$(echo $REGISTRY_USER)" \
    --docker-password="$(echo $REGISTRY_PASS)"

